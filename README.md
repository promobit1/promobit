# Как запускать:

Устанавливаем ansible на рабочий комьютер (локальный). В терминале:

    sudo apt update
    sudo apt install ansible

Копируем к себе репозиторий

    git pull git@gitlab.com:promobit1/promobit.git

Если на nginx сервере будет HTTPS (сертификат), то дополнительно придется установить sshpass для кооректной работы (ошибка в консоли подскажет установить этот пакет). Запуск команды плейбука:

    sudo ansible-playbook nginx_playbook.yml -kK

## Описание структуры проекта

    inventory
        hosts.ini - здесь хранятся используемые хосты, к которым обращается ansible в nginx_playbook.yml

    roles
        nginx_install - это роль установки сервера и копирования файла index.html
            main.yml - задача

    files - файлы которые подразумевались к копированию
        ...

    ansible.cfg - для того чтобы не вводить парамтер -i (-i inventory/hosts.ini) каждый раз при запуске плейбука - укажем его в конфигурации ансибл

    nginx_playbook.yml это плейбук для запуска ансибл
